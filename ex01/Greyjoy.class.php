<?php

    /*
    ** JavaScript doesn't have protected members for inheritance, so this ex
    ** had to be done in another language.
    */

    class Greyjoy
    {
        protected $familyMotto = "We do not sow";
    }
