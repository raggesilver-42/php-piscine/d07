<?php

    class NightsWatch implements IFighter
    {
        private $recruits;

        public function recruit($some)
        {
            if ($some instanceof IFighter)
                $this->recruits[] = $some;
        }

        public function fight()
        {
            foreach ($this->recruits as $key => $value)
                $value->fight();
        }
    }
